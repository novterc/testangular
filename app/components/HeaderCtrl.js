angular.module('myApp')
    .controller('HeaderCtrl', ['$scope', 'Wheather', function ($scope, Wheather) {

        $scope.cityName = '';
        watchWheather('cityName');

        $scope.description = '';
        watchWheather('description');

        $scope.icon = '';
        watchWheather('icon');

        function watchWheather(fieldName, scopeName) {
            if (scopeName === undefined) {
                scopeName = fieldName;
            }
            $scope.$watch(function () {
                return Wheather[fieldName]
            }, function (newValue) {
                $scope[scopeName] = newValue;
            }, true);
        }

    }]);
