angular.module('myApp')
    .controller('ContentCtrl', ['$scope', 'Geolocation', 'LolcalStorage', 'WheatherApi', 'Wheather',
        function ($scope, Geolocation, LolcalStorage, WheatherApi, Wheather) {

            $scope.submitCityNameForm = function (cityName) {
                WheatherApi.getByCityName(cityName)
                    .then(LolcalStorage.setWheather)
            };

            if (LolcalStorage.getWheather() === false) {
                Geolocation.getCoordinates(function (lat, lon) {
                    WheatherApi.getByCoordinates(lat, lon)
                        .then(LolcalStorage.setWheather)
                });
            }

            setInterval(function () {
                if (Wheather.getLostMinut() >= 30) {
                    WheatherApi.getRefrash()
                        .then(LolcalStorage.setWheather)
                }
            }, 1000 * 60);

        }]);
