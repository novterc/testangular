angular.module('myApp')
    .service('LolcalStorage', ['Wheather', function (Wheather) {

        var LolcalStorage = this;

        LolcalStorage.getWheather = function () {
            var wheatherData = LolcalStorage.getObj('wheather');
            if (wheatherData === null) {
                return false;
            }
            Wheather.setCityName(wheatherData.cityName);
            Wheather.setDescription(wheatherData.description);
            Wheather.setIcon(wheatherData.icon);
            Wheather.setCoord(wheatherData.coord);
            Wheather.setActualDate(wheatherData.actualDate);
            return true;
        };

        LolcalStorage.setWheather = function () {
            console.log(Wheather.getData());
            LolcalStorage.setObj('wheather', Wheather.getData());
        };

        LolcalStorage.getObj = function (key) {
            var json = localStorage.getItem(key);
            if (json === undefined) {
                return null;
            }

            try {
                var obj = JSON.parse(json);
                return obj;
            } catch (e) {
                return null;
            }
        };

        LolcalStorage.setObj = function (key, obj) {
            var objJson = JSON.stringify(obj);
            localStorage.setItem(key, objJson);
        };

    }]);
