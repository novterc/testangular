angular.module('myApp')
    .service('Geolocation', function () {

        var Geolocation = this;

        Geolocation.getCoordinates = function (success, error) {
            if (!navigator.geolocation) {
                if (error !== undefined) {
                    error('Geolocation is not supported by your browser');
                }
                return;
            }

            navigator.geolocation.getCurrentPosition(function (position) {
                success(position.coords.latitude, position.coords.longitude);
            }, function () {
                if (error !== undefined) {
                    error('Unable to retrieve your location');
                }
            });
        };

    });
