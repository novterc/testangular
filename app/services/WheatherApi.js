angular.module('myApp')
    .service('WheatherApi', ['$http', 'Wheather', function ($http, Wheather) {

        var WheatherApi = this;
        WheatherApi.apiKey = '9a9b29c17341e46594a57854498c8872';

        WheatherApi.getByCoordinates = function (lat, lon) {
            return new Promise(function (resolve, reject) {
                var requestUrl = 'http://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + lon + '&appid=' + WheatherApi.apiKey;
                $http.get(requestUrl)
                    .then(function (response) {
                        WheatherApi.responseHandler(response, resolve, reject);
                    }, reject);
            });
        };

        WheatherApi.getByCityName = function (cityName) {
            return new Promise(function (resolve, reject) {
                var requestUrl = 'http://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&appid=' + WheatherApi.apiKey;
                $http.get(requestUrl)
                    .then(function (response) {
                        WheatherApi.responseHandler(response, resolve, reject)
                    }, reject);
            });
        };

        WheatherApi.responseHandler = function (response, resolve, reject) {
            if (response.status !== 200) {
                if (reject !== undefined) {
                    reject('invalid response status');
                }
                return
            }

            Wheather.setCityName(response.data.name);
            Wheather.setDescription(response.data.weather[0].description);
            Wheather.setIcon('http://openweathermap.org/img/w/' + response.data.weather[0].icon + '.png');
            Wheather.setCoord(response.data.coord);
            Wheather.setActualDate();

            if (resolve !== undefined) {
                resolve(response);
            }
        };

        WheatherApi.getRefrash = function () {
            return new Promise(function (resolve, reject) {
                if (Wheather.coord !== null) {
                    WheatherApi.getByCoordinates(Wheather.coord.lat, Wheather.coord.lon).then(resolve, reject);
                } else {
                    reject();
                }
            });
        }

    }]);
