angular.module('myApp')
    .service('Wheather', function () {

        var Wheather = this;
        Wheather.cityName = '';
        Wheather.description = '';
        Wheather.icon = '';
        Wheather.actualDate = null;
        Wheather.coord = null;

        Wheather.setCityName = function (name) {
            Wheather.cityName = name;
        };

        Wheather.setDescription = function (description) {
            Wheather.description = description;
        };

        Wheather.setIcon = function (icon) {
            Wheather.icon = icon;
        };

        Wheather.setCoord = function (coord) {
            Wheather.coord = coord;
        };

        Wheather.setActualDate = function (date) {
            if (date === undefined) {
                var dateObj = new Date();
            } else {
                var dateObj = new Date(date);
            }
            Wheather.actualDate = dateObj.getTime();
        };

        Wheather.getLostMinut = function () {
            var diff = Math.abs(Wheather.actualDate - new Date());
            var minutes = Math.floor((diff / 1000) / 60);
            return minutes;
        };

        Wheather.getData = function () {
            var listValue = {};
            for (var key in this) {
                var value = this[key];
                if (Object.prototype.hasOwnProperty.call(this, key) && typeof value !== "function") {
                    listValue[key] = value;
                }
            }
            return listValue;
        };

    });
